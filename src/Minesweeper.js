const bombMatrix =
  [
    [0, 1, 0, 0],
    [0, 1, 1, 0],
    [0, 0, 0, 0],
    [0, 0, 0, 1],
  ];

const Minesweeper = function () {
  this.status = null; // 'win', 'lose', 'in-progress'
  this.grid = null; // user-facing matrix
  this.matrix = null; // input matrix
  this.bombCount = null; // number of bombs in the matrix
  this.revealedCells = null; // number of revealed cells
};

Minesweeper.prototype.setNewGame = function (matrix) {
  // initializes game.status and game.grid
  this.status = 'in-progress';
  this.matrix = matrix;
  this.grid = matrix.map(row => row.map(() => '_'));
  console.log(this.grid);
  this.bombCount = matrix.reduce(
    (count, rowArr) => rowArr.reduce(
      (countRow, value) => countRow + value,
      count
    ),
    0
  );
  console.log('bomb count:', this.bombCount);
  this.revealedCells = 0;
};

Minesweeper.prototype.newGame = function (rows, cols, density = 0.5) {
  console.log('newGame', { rows, cols, density });
  const matrix = [];
  for (let r = 0; r < rows; r += 1) {
    matrix.push([]);
    for (let c = 0; c < cols; c += 1) {
      const isBombInner = Math.random() + density;
      console.log({ isBombInner });
      matrix[matrix.length - 1].push(Math.min(Math.floor(isBombInner), 1));
    }
  }
  this.setNewGame(matrix);
};

Minesweeper.prototype.setGridCell = function (row, col, value) {
  if (this.grid[row][col] === '_') {
    this.revealedCells += 1;
    this.grid[row].splice(col, 1, value);
  } else {
    console.log(
      `Ignoring cell update; cell already set to ${this.grid[row][col]}`,
      { row, col, value },
    );
  }
};

Minesweeper.prototype.checkCell = function (row, col) {
  // reveals number of neighbor cells with a bomb
  this.setGridCell(row, col, this.cellDisplay(row, col));
  if (this.matrix[row][col]) {
    // Bomb!
    this.status = 'lose';
    this.revealAll();
  } else if (this.revealedCells === (this.matrix.length * this.matrix[0].length) - this.bombCount) {
    // All non-bomb cells revealed: you win!
    this.status = 'win';
    this.revealAll();
  } else if (this.grid[row][col] === '0') {
    this.fillEmptyArea(row, col);
  }
  console.log('game status:', this.status);
  console.log(this.grid);
};

Minesweeper.prototype.fillEmptyArea = function (row, col) {
  this.getNeighbors(row, col)
    .forEach(([r, c]) => {
      if (this.grid[r][c] !== '_') {
        return;
      }

      this.setGridCell(r, c, this.cellDisplay(r, c));
      if (this.grid[r][c] === '0') {
        this.fillEmptyArea(r, c);
      }
    });
};

Minesweeper.prototype.revealAll = function () {
  this.grid = this.matrix
    .map((rowArr, r) => rowArr.map((colArr, c) => this.cellDisplay(r, c)));
};

Minesweeper.prototype.cellDisplay = function (row, col) {
  // Get the character to display for the given (revealed) cell
  if (this.matrix[row][col]) {
    return 'B';
  }
  return this.countAdjacentBombs(row, col).toString();
};

Minesweeper.prototype.countAdjacentBombs = function (row, col) {
  // counts number of neighbor cells with a bomb
  return this.getNeighbors(row, col)
    .reduce((count, neighbor) => {
      if (this.matrix[neighbor[0]][neighbor[1]]) {
        return count + 1;
      }
      return count;
    }, 0);
};

Minesweeper.prototype.getNeighbors = function (row, col) {
  // get array of neighbor cell coordinates
  const neighbors = [];
  if (row > 0) {
    neighbors.push([row - 1, col]);
    if (col > 0) {
      neighbors.push([row - 1, col - 1]);
    }
    if (col < this.matrix[0].length - 1) {
      neighbors.push([row - 1, col + 1]);
    }
  }
  if (row < this.matrix.length - 1) {
    neighbors.push([row + 1, col]);
    if (col > 0) {
      neighbors.push([row + 1, col - 1]);
    }
    if (col < this.matrix[0].length - 1) {
      neighbors.push([row + 1, col + 1]);
    }
  }
  if (col > 0) {
    neighbors.push([row, col - 1]);
  }
  if (col < this.matrix[0].length - 1) {
    neighbors.push([row, col + 1]);
  }
  return neighbors;
};

function test() {
  const game = new Minesweeper();
  game.setNewGame(bombMatrix);

  // play!

  // case 1: One or more neighbor has a bomb
  game.checkCell(0, 0);
  // game.grid =>
  [
    ['2', '_', '_', '_'],
    ['_', '_', '_', '_'],
    ['_', '_', '_', '_'],
    ['_', '_', '_', '_']
  ];

  // case 2: No neighbors have a bomb
  game.checkCell(3, 0);
  // game.grid =>
  [
    ['2', '_', '_', '_'],
    ['_', '_', '_', '_'],
    ['1', '2', '3', '_'],
    ['0', '0', '1', '_']
  ];
  game.checkCell(3, 0);game.checkCell(3, 0);game.checkCell(3, 0);game.checkCell(3, 0);game.checkCell(3, 0);game.checkCell(3, 0);game.checkCell(3, 0);game.checkCell(3, 0);game.checkCell(3, 0);game.checkCell(3, 0);game.checkCell(3, 0);game.checkCell(3, 0);

  // case 3: Checked cell has a bomb... you lose :(
  game.checkCell(3, 3); // reveals all cells and sets game.status to 'lose'
  // game.grid =>
  [
    ['2', 'B', '3', '1'],
    ['2', 'B', 'B', '1'],
    ['1', '2', '3', '2'],
    ['0', '0', '1', 'B']
  ];

  // case 4: Last cell without a bomb is checked... you win!
  // game.checkCell(?, ?) // reveals all cells (only bombs at this point) and sets game.status to 'win'
}

export default Minesweeper;
